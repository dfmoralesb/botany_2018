### Baited homology search using SWIPE

Navigate to the tutorial directory ~/phylogenomic_dataset_construction/examples/bait 

	cd ~/Desktop/botany_2018/examples/bait

The bait fasta file we are using is the fasta file UFGT.pep.fa. It contains flavonoid-3-O-glucosyltransferase from Arabidopsis thaliana. This gene codes for an enzyme involved in anthocyanin biosynthesis and plant coloration. Arabidopsis has four copies of UFGT due to two rounds of recent genome duplications. The script uses these 4 baits to search each of the 5 Caryophyllales taxon and take the top 10 hits per taxon. Type
	
	python ~/Desktop/botany_2018/scripts/bait_homologs.py

It tells you what are the inputs needed. We are not going to actually run the pipeline given the time constraint. But the command we ran ahead of time was

	python ~/Desktop/botany_2018/scripts/bait_homologs.py UFGT.pep.fa data 10 2

For each taxon, the script outputs the blastp and swipe results in the data directory. The swipe output columns are

Query id, Subject id, % identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score

The blastp output columns are

qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore

For each blastp and swipe outputs, the scripts sort the hits from all 4 bait sequences by bitscore from high to low, and write the top 10 subject ids in the .hits file. Open the .hits files and compare the ranking between blastp.hits vs. swipe.hits. For example

	head data/SCAO*hits

The rank by swipe vs. blastp are slightly different. The scripts also output a file UFGT\_blastp.fa with all the top ranked sequences plus baits, and a file UFGT\_swipe.fa with the top swipe hits plus baits. Make an alignment and a tree from each fasta file using the scipt "fasta_to_tree.py". Again type the python command first without giving it any input

	python ~/Desktop/botany_2018/scripts/fasta_to_tree_pxclsq.py

It tells you that you need to give it: the input directory where the fasta files are, number of cores, whether the fasta files are DNA or amino acides, and wether you want to do bootstrap. Again we ran this ahead of time for you but this is what we did

	python ~/Desktop/botany_2018/scripts/fasta_to_tree_pxclsq.py ~/Desktop/botany_2018/examples/bait 2 aa n

You can visualize the .tre files using figtree. **In the terminal of the GUI type:**

	cd ~/Desktop/botany_2018/examples/bait
	
	figtree UFGT_swipe.raxml.tre
	
You can find where the Arabidopsis baits are by searching for Athaliana. Our transciptome data set consisted of an Amaranthaceae outgroup (Beta), and the rest are ingroups (Molluginaceae from 1KP) 

You can further extract subclades containing the baits, write the fasta file of the subclade, and refine the alignment and tree, and carry out dN/dS analysis etc but we'll stop here for this tutorial.