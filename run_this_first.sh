#!/bin/bash


echo 'export PATH="$PATH:/usr/local/bin/Salmon-0.8.2_linux_x86_64/bin"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/Salmon-latest_linux_x86_64/bin"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/ncbi-blast-2.2.30+/bin"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/standard-RAxML"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/sratoolkit.2.9.1-1-ubuntu64/bin"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/trinityrnaseq-Trinity-v2.5.1"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/samtools-1.9"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/phyx/src"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/paup"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/phyutility"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/swipe-2.0.5"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/bowtie-1.2.2-linux-x86_64"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/bowtie2-2.3.4.1-linux-x86_64"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/hmmer-3.2.1/src"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/FastQC"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/corset"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/TreeShrink"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/Transdecoder"' >> ~/.bashrc
echo 'export PATH="$PATH:/usr/local/bin/cd-hit-v4.6.8-2017-1208"' >> ~/.bashrc
transrate --install-deps read
rm ~/.local/bin/salmon
vncserver
